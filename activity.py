class Node:

	def __init__(self, data):
		self.data = data
		self.next = None

class ColorLinkedList:
	def __init__(self):
		self.head = None

	
	def prepend(self, data):
		new_node = Node(data)
		
		new_node.next = self.head
		
		self.head = new_node; 


	
	def append(self, data):

		new_node = Node(data)

		if self.head is None:
			self.head = new_node
			return

		node = self.head

		while node.next:
			node = node.next

		last_node = node

		last_node.next = new_node


	def delete_node(self, key):

		current_node = self.head
		# print("current node: " + current_node.data)

		while current_node and current_node.data != key:
			previous = current_node
			current_node = current_node.next

		# print("Matched Current Node: " + current_node.data)
		
		if current_node is None:
			return

		previous.next = current_node.next
		
		current_node = None
		# print(current_node)


	def print_list(self):

		current_node = self.head

		
		while current_node:
			print(current_node.data)

			
			current_node = current_node.next

	def search_color(self, key):

		current_node = self.head
		present = False
		
		while current_node:
			if(current_node.data == key):
				present = True

			
			current_node = current_node.next
		exist = "No"
		if(present):
			exist = "Yes"
		print(exist)

	def count_node(self):

		current_node = self.head
		count = 0
		
		while current_node:
			count = count + 1

			
			current_node = current_node.next
		print(f'Total number of nodes in the linked list : \n{count}')

llist = ColorLinkedList()
llist.append("Brown")
llist.append("Green")
llist.append("Blue")
llist.append("Grey")
llist.append("Black")
llist.print_list()
print("\n")
llist.prepend("Purple")
print("Insert the color 'Purple' at the front of the linked list\n")
llist.print_list()
print("\n")
print('Delete the color "Black" from the linked list:')
llist.delete_node("Black")
llist.print_list()
print("\n")
print('Does the color "Grey" exists in the linked list?')
llist.search_color("Grey")
print("\n")
print('Does the color "Black" exists in the linked list?')
llist.search_color("Black")
print("\n")
llist.count_node()